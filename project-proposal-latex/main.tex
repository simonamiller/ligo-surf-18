\documentclass[reprint, amsmath,amssymb,aps,]{revtex4-1}

\usepackage{graphicx}% Include figure files
\usepackage{float}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{blindtext}

\begin{document}

\preprint{APS/123-QED}

\title{Detecting and Measuring Eccentric Black Hole Binaries\\
\small 
\textit{\\Project Proposal, LIGO Student Undergraduate Research Fellowship, California Institute of Technology}}

\author{Simona Miller}
\affiliation{Smith College, Northampton, MA 01063}

\author{Mentors: Alan Weinstein and Jonah B. Kanner}
\affiliation{LIGO Laboratory, Caltech, Pasadena, CA 91125\vspace{0.4cm}}

\date{\today}


% ABSTRACT
\begin{abstract}
If a binary black hole system resulted from dynamical capture close to the end of its life, there is a probability that the system could still be in eccentric orbit while in the LIGO frequency band. As such, observing eccentricity from a gravitational wave signal would be a clear signature of dynamical capture. We seek to discover if the LIGO detectors are capable of detecting gravitational radiation from BBH in eccentric orbit in the LIGO band (GW frequency from 20 to ~3000 Hz, corresponding to orbital frequency of $>$ 10 Hz). We will assess how much SNR will be lost when using a quasi-circular waveform template to detect a BBH with eccentricity. Using Bayesian inference, we will work to extract the eccentricity parameter from a GW signal, assuming all other parameters are known. The results from this project will indicate how much eccentricity affects measurements of GW from BBH. Additionally, results will provide important insight into the nature of binary black holes: do they typically evolve together (as indicated by a circularized orbit) or are they more often a result of dynamical capture close to the end of their life? 
\end{abstract}

\maketitle


%INTRODUCTION
\section{Introduction}
I will be joining LIGO's gravitational wave burst group, working to detect and identify eccentricity of binary black hole mergers. While several groups in the LIGO Scientific Collaboration have worked with the ``eccentricity problem", many important questions remain to be answered quantitatively \cite{tiwari}. Presently, gravitational wave detection uses templates that assume a negligible eccentricity; highly eccentric BBH could go undetected with current technology. Furthermore, many properties of BBH  are currently calculated with the assumption of circular orbits, such as their distance from Earth \cite{sath}. Identifying eccentricity could make such calculations more accurate.  

To date, LIGO's detectors have observed gravitational waves from binary black holes that follow a consistent pattern: the BBH systems have circular orbits that decrease in radius and increase in frequency as they lose energy in the form of gravitational radiation. All observed gravitational waves from BBH fit a ``chirp" waveform while within the LIGO frequency band; their amplitude and frequency increase as the binary evolves \cite{sath}. Figure 1 shows predicted chirp waveforms and Figure 2 shows the actual waveform from GW150914, LIGO's first detected gravitational wave. 

\begin{figure}[h!]
\centering
\includegraphics[width=0.5\textwidth]{chirp_waveform}
\caption{Chirp waveform predicted using effective-one-body approach (left) and numerical relativity simulations (right) for same initial conditions. Figure from \cite{sath}}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[width=0.4\textwidth]{GW150914}
\caption{Strain (h(t)) plotted against time (t) for GW150914, LIGO's first GW detection. L1 is the data from the Livingston, Louisiana detector; H1 is the data from the Hanford, Washington detector. Figure from \cite{ligo_website}}
\end{figure}

Binary black holes have two known formation mechanisms: isolated binary evolution and dynamical interaction in dense stellar environments. Evolutionary trajectories for both formation mechanisms predict that under most circumstances, the orbits of BBH systems will have circularized by the time their emitted gravitational radiation is within the LIGO band. However, if a BBH forms via dynamical capture with an extremely large eccentricity and/or extremely close to the end of its lifetime (i.e., a small periapse), there \emph{is} a probability that the system could be detected using quasi-circular templates while still in a non-circular orbit \cite{abbott}. This probability will be calculated in this project. 

Dense stellar regions, such as galactic nuclei and globular clusters that have undergone core collapse, are prime spots for dynamical BH-BH capture. In such settings, individual black holes can become gravitationally bound during close passage as energy is lost due in the form of a GW burst\cite{east}. Samsing predicts that up to 5\% of the observable BBH forming in globular clusters have an eccentricity $>$ 0.1 \cite{samsing}. Once a dynamically captured BH pair is in eccentric orbit, a GW burst is theorized to be emitted every time the pair passes at a close encounter (i.e. at its periapse). This causes the periapse ($r_p$) and eccentricity ($e$) to decrease with time, while orbital frequency increases with time. After sufficient energy is lost through gravitational radiation, the BH pair will merge. 

Several groups have created waveform models for eccentric BBH. For example, East et al. created a model combining inspiral and merger models to predict a full waveform for dynamical capture binaries. Their results are shown in Figure 3. While this model can be applied where standard Post-Newtownian waveforms fail, i.e., the late stages of merger, it is still insufficient to be used in matched filtering \cite{east}. This is because the template ignores spin and has too large of a template bank due to a large number of parameters describing it. 

\begin{figure}[h]
\centering
\includegraphics[width=0.45\textwidth]{dynamic_capture_merger}
\caption{Gravitational wave strain plot generated by the model described in East et al.~ for a periapse $r_p = 0.8M$ and an eccentricity e = 1. The top panel shows the entire waveform; the bottom panel shows a zoomed-in view of the end of the waveform. Figure from \cite{east}}
\end{figure}

% OBJECTIVES
\section{Objectives}
The aim of this project is to assess detectability and identifiability of eccentric binary black holes. We will focus on two questions: 
\begin{enumerate}
\item Does eccentricity make GW signals from BBH harder to detect? Specifically, how much SNR can be lost if a search pipeline uses quasi-circular binary templates to capture an eccentric binary merger event? We will calculate this as a function of the initial eccentricity (at GW frequency of 20 Hz) and the masses of the two black holes (and potentially their spins). 

\item How well can the eccentricity parameter be extracted from an observed event, using Bayesian parameter estimation?
\end{enumerate}



% APPROACH
\section{Approach}
To achieve the objectives described above we will do the following: 
\begin{enumerate}[label=(\Alph*)]
\item Perform a literature search to look for what models currently exist for eccentric BBH waveforms. 
\item Calculate the expected eccentricity ($e_0$) distribution for BBH in the LIGO band 
\item Model a waveform for an eccentric BBH in Python using first order Parametrized Post-Newtownian formalism, focusing on the inspiral phase. Create a waveform family for BBH over a range of masses and initial eccentricity.
\item Use this waveform to create an eccentric BBH template that could be used for matched filtering; Compare its overlap against quasi-circular orbits. Calculate how much SNR is lost when using a quasi-circular template to detect an eccentric BBH.
\item Apply Bayesian Inference and Markov Chain Monte Carlo analysis to estimate the value of the eccentricity parameter in a data set. Use these techniques to plot eccentricity vs. chirp mass.
\item Focus our efforts on applying these methods to future (3G) LIGO detectors. Explore what these detectors could accomplish that current detectors cannot, in terms of detecting GW from eccentric BBH and providing data from which eccentricity is identifiable. 
\end{enumerate}

% PROJECT SCHEDULE 
\section{Project Schedule}
\begin{description}[align=left]
\item [Before arrival] Become familiar with LIGO data analysis methods - i.e. parameter estimation, MCMC, and Bayesian inference - using LIGO Scientific Collaboration's Open Data Workshop online tutorials. 
\item [Week 1] Literature review - accumulate list of existing models for eccentric BBH waveforms. Begin learning how to apply Parametrized-Post Newtownian Formalism to calculating waveforms. 
\item [Week 2] Calculate expected eccentricity distribution of BBH in the LIGO band. Begin writing code in Python to model waveforms. 
\item [Week 3-4] Model waveforms with different eccentricities and mass ratios. Write first interim report. 
\item [Week 5] Create eccentric BBH template for matched filtering.
\item [Weeks 6-7] Complete comparison of SNR for eccentric BBH template with SNR for matched filtering against quasi-circular orbits. Calculate how much SNR is lost when using a quasi-circular template to detect an eccentric BBH. Begin applying Bayesian inference and MCMC. Write second interim report. 
\item [Week 8] Continue using Bayesian inference and MCMC analysis to estimate the value of the eccentricity parameter in a data set. Plot eccentricity vs. chirp mass.
\item [Week 9] Look into applying our techniques to data from future (3G) detectors. Work on final presentation. 
\item [Week 10] Tie up loose ends. Finalize final presentation.
\end{description}


% REFERENCES: 
\begin{thebibliography}{15}

\bibitem{abbott}
Abbott, B., et al. (LIGO Scientific Collaboration), "Astrophysical Implications of the Binary Black-Hole Merger GW150914," \emph{Astrophys. J.}, LIGO-P1500262, (2016), [arXiv:1602.03846v1].

\bibitem{east}
East, William B., et al. "Observing complete gravitational wave signals from dynamical capture binaries", \emph{Phys. Rev. D},  87 (2013), [arXiv:1212.0837v2]

\bibitem{samsing}
Samsing, Johan. "Eccentric Black Hole Mergers Forming in Stellar Clusters, (2017), [arXiv:1711.07452]

\bibitem{sath}
Sathyaprakasch, B.S and Bernard F. Schutz, “Physics, Astrophysics, and Cosmology with Gravitational Waves,”  \emph{Living Rev. Relativity}, lrr-2009-2, (2009): http://www.livingreviews.org/lrr-2009-2.

\bibitem{tiwari}
Tiwari, V., et al. "A Proposed Search for the Detection of Gravitational Waves from Eccentric Binary Black Holes," \emph{Phys. Rev. D}, 93 (2016), [arXiv:1511.09240] 

\bibitem{ligo_website}
LIGO Open Science Center, Data release for event GW150914, https://losc.ligo.org/events/GW150914/



\end{thebibliography}

\end{document}
