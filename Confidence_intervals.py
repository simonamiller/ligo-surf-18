
# SNR & Eccentricity vs. Confidence that ecc!=0

# Simona Miller, LIGO SURF 2018 (Last edited: August 7, 2018)

import numpy as np
import matplotlib as mpl
mpl.use('Agg')
from matplotlib import pyplot as plt

from pycbc import psd as pypsd
from pycbc.waveform.generator import FDomainDetFrameGenerator, FDomainCBCGenerator
from pycbc import inference

from matplotlib import rc
rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)
rc('xtick', labelsize=18) 
rc('ytick', labelsize=18)


# Global variables
seglen = 10
sample_rate = 2048
N = seglen*sample_rate/2+1
fmin = 20.

mass, tsig, ra, dec, pol = 20., 3.1, 1.37, -1.26, 2.76

true_ecc = np.arange(0.001,0.1,0.001)

de = 0.001
ecc_range = np.arange(0.0, 0.2, de)

snr_range = np.arange(5,100,0.5)



# Waveform generator
generator = FDomainDetFrameGenerator(FDomainCBCGenerator, 
                                     0., 
                                     variable_args=['eccentricity', 'distance'], 
                                     detectors=['H1', 'L1'], 
                                     delta_f=1./seglen, 
                                     f_lower=fmin, 
                                     approximant='EccentricFD', 
                                     mass1 = mass, 
                                     mass2 = mass,
                                     tc=tsig, 
                                     ra=ra, 
                                     dec=dec, 
                                     polarization=pol)

# PSD
psd = pypsd.analytical.aLIGODesignSensitivityP1200087(N, 1./seglen, 20.)
psds = {'H1': psd, 'L1': psd}


# --- Necessary Helper Functions ---

def SNRtoDistance(SNR, ecc, mass, psd=psd, f_lower=fmin):

    data0 = generator.generate(eccentricity=ecc, mass1=mass, mass2=mass, distance=1)
    
    likelihood_eval = inference.GaussianLikelihood(['eccentricity'],
                                                       generator, data0, fmin, psds=psds, return_meta=False)
    SNR0 = likelihood_eval.snr()
        
    distance = int(float(SNR0)/SNR)
    
    return distance


def Normalize(lnlik, de): 
    
    norm_const = 1.0/(np.sum(np.exp(lnlik[:]))*de)
    
    norm = [norm_const*np.exp(x) for x in lnlik]
 
    return norm 

# Returns the confidence level at which x!=0
# **Assuming first element in data corresponds to x=0**

def confidence_not_zero(data, dx):
    z = data[0]
    
    data_s = sorted(data, reverse=True)
    idx = data_s.index(z)
    
    confidence = 0
    for d in data_s[:idx]: 
        confidence+=(d*dx)

    return confidence  


# --- Confidence! ---

confidence = {}

for SNR in snr_range:
    for e0 in true_ecc:
        
        dist = SNRtoDistance(SNR, e0, mass)

        signal = generator.generate(eccentricity=e0, distance=dist)
        
        likelihood_eval = inference.GaussianLikelihood(['eccentricity'],
                                                       generator, signal, fmin, psds=psds, return_meta=False)
        
        lnlik = []
        for e in ecc_range:    
            lnlik.append(likelihood_eval.loglr(eccentricity=e))
        
        norm = Normalize(lnlik,de)
        print 'Generated distribution for SNR={}, e0={}'.format(SNR,e0)
            
        confidence[(SNR, e0)] = confidence_not_zero(norm,de)
        print 'Calculated confidence that e!=0 for SNR={}, e0={}'.format(SNR,e0)
    

# Plotting 
  
SNR_inj = np.transpose(confidence.keys())[0]
ecc_inj = np.transpose(confidence.keys())[1]
conf = confidence.values()

#fig = plt.figure(figsize=(10,7))    
#ax = plt.gca()
#plt.scatter(ecc_inj, SNR_inj , c=conf, s=100, alpha=0.7, cmap='Reds')
#plt.xlabel(r'Injected eccentricity $(e_0)$',fontsize=20)
#plt.ylabel('SNR',fontsize=20)
#ax.set_yscale('log')
#ax.set_xscale('log')
#cb = plt.colorbar()
#cb.set_label(label=r'confidence that $e_0\neq$0',fontsize=20)
#plt.savefig('confidence_loglog.png')
#plt.show()

f = open('confidence_plt.txt', 'w')

for i in range(len(SNR_inj)): 
    snr_write = SNR_inj[i]
    ecc_write = ecc_inj[i]
    conf_write = conf[i]
    line = str(snr_write) + ' ' + str(ecc_write) + ' '  + str(conf_write) + '\n'
    f.write(line)

f.close()

    







