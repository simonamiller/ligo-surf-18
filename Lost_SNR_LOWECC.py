import pycbc.noise
import pycbc.psd
from pycbc.filter import get_cutoff_indices
from pycbc.filter.matchedfilter import matched_filter
from pycbc.waveform import get_td_waveform
from pycbc.psd import interpolate

import numpy as np
from scipy.signal.windows import tukey

import os 
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt


from matplotlib import rc
rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)
rc('xtick', labelsize=14) 
rc('ytick', labelsize=14)


# ## Noise PSD

# The color of the noise matches a PSD which you provide
f_lower = 20.0
f_upper = 1024.0
delta_f = 1.0 / 16
flen = int(2048 / delta_f) + 1
psd = pycbc.psd.analytical.aLIGODesignSensitivityP1200087(flen, delta_f, f_lower)

# Generating 16 seconds of noise at 2048 Hz
delta_t = 1.0 / 2048
tsamples = int(16 / delta_t)
noise_ex = pycbc.noise.noise_from_psd(tsamples, delta_t, psd, seed=127)



# ## Functions: gen_ecc_waveform, injection

def gen_ecc_waveform(theta_ecc,
                     delta_t=delta_t,
                     delta_f=delta_f,
                     f_lower=f_lower,
                     duration=noise_ex.duration,
                     start_time=noise_ex.start_time):
    
    eccentricity, mass = theta_ecc
    
    hp, hc = get_td_waveform(approximant="EccentricTD",
                             mass1=mass,
                             mass2=mass,
                             eccentricity=eccentricity,
                             delta_t=delta_t,
                             delta_f=delta_f, 
                             f_lower=f_lower, 
                             distance=1000)
    
    return hp


def Injection(theta_ecc): 
    
    # Generate noise
    noise = pycbc.noise.noise_from_psd(tsamples, delta_t, psd, seed=np.random.randint(100))
    
    # Generate waveform/template to be injected
    hp = gen_ecc_waveform(theta_ecc)
    
    # Window for template
    window_template = tukey(len(hp), alpha=0.01)
    for i in range(len(hp)):
        hp[i] = hp[i]*window_template[i]
    
    # Resizing template to match noise
    hp.resize(len(noise))
    template = hp.cyclic_time_shift(hp.start_time-8) # waveform injected halfway through noise
    template._epoch = 0 # setting start time to 0
    
    # Adding template and noise 
    template_with_noise = template + noise
    
    # Window for signal with noise
    window_noise = tukey(len(template_with_noise), alpha=0.01)
    for i in range(len(template_with_noise)):
        template_with_noise[i] = template_with_noise[i]*window_noise [i]
        
    return template_with_noise



if __name__ == "__main__":

    # SNR lost when recovering with non-eccentric waveform
    
    noecc = 1e-10
    ecc_range = np.arange(0.001,0.1,0.001)
    mass_range = np.arange(5,30)

    lost_snr = {}

    for m in mass_range:
        for e in ecc_range:
        
            theta_e = (e,m)
            theta_noe = (noecc,m)
        
            # Data
            inj = Injection(theta_e)
        
            # Template with eccentricity
            template_e = gen_ecc_waveform(theta_e)
            template_e.resize(len(inj))
        
            # Template without eccentricity
            template_noe = gen_ecc_waveform(theta_noe)
            template_noe.resize(len(inj))
        
            # Calculating SNRs
            snr_e = np.absolute(matched_filter(template_e,inj,psd=psd,low_frequency_cutoff=f_lower))
            snr_noe = np.absolute(matched_filter(template_noe,inj,psd=psd,low_frequency_cutoff=f_lower))
        
            lostsnr = (max(snr_e)-max(snr_noe))/max(snr_e)
        
            lost_snr[theta_e] = lostsnr

            print "Ratio of lost SNR for m={}, e={} is {}".format(m,e,lostsnr)


    ecc_plt = np.transpose(lost_snr.keys())[0]
    mass_plt = np.transpose(lost_snr.keys())[1]
    lost_snr_plt = lost_snr.values()

    plt.figure(figsize=(7,7))
    plt.scatter(mass_plt, ecc_plt, s=200, c=lost_snr_plt , cmap='Blues')
    plt.title('Ratio of SNR Lost', fontsize=18)
    plt.ylabel('Eccentricity', fontsize=16)
    plt.xlabel('Mass (solar mass units)', fontsize=16)
    plt.colorbar()
    plt.savefig('lost_snr_lowecc.png')
