import numpy as np
from gwpy.timeseries import TimeSeries
import argparse
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

def plotFrame(frame,channel,figname):

    # Load frame data
    data = TimeSeries.read(frame,channel)
    print data.epoch

    # Plot data
    frameFigure = data.plot()
    frameFigure.savefig(figname)

if __name__=="__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-s',help='Source frame file')
    parser.add_argument('-c',help='Source frame channel')
    parser.add_argument('-t',help='Target figure name')
    args = parser.parse_args()

    plotFrame(args.s,args.c,args.t)

 
