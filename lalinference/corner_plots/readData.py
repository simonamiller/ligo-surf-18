import numpy as np
import argparse
from spins import *
import h5py as h5

def readSamples(h5file):

    # Open file
    f = h5.File(h5file,'r')

    # Get SNR
    snr = np.max(f['lalinference/lalinference_nest/posterior_samples']['matched_filter_snr'])

    # Get mass samples
    Mc = f['lalinference/lalinference_nest/posterior_samples']['mc']
    q = f['lalinference/lalinference_nest/posterior_samples']['q']
    Mtot = (1.+q)*np.power((1.+q)/(q**3.),1./5.)*Mc
    m2 = Mtot*q/(1.+q)
    m1 = Mtot/(1.+q)

    # Distance
    logD = f['lalinference/lalinference_nest/posterior_samples']['logdistance']
    dist = np.exp(logD)

    # Get spins
    a1 = f['lalinference/lalinference_nest/posterior_samples']['a1']
    a2 = f['lalinference/lalinference_nest/posterior_samples']['a2']
    tilt1 = f['lalinference/lalinference_nest/posterior_samples']['tilt1'] #angle (rad) between L and S
    tilt2 = f['lalinference/lalinference_nest/posterior_samples']['tilt2']

    # Compute chiEffective and chiP
    chiEffective = getChiEffective(m1,m2,a1,a2,tilt1,tilt2)
    chiP = getChiP(m1,m2,a1,a2,tilt1,tilt2)

    data = {}
    data['snr'] = snr
    data['Mtot'] = Mtot
    data['Mc'] = Mc
    data['q'] = q
    data['a1'] = a1
    data['a2'] = a2
    data['chiEffective'] = chiEffective
    data['chiP'] = chiP
    data['dist'] = dist

    #return snr,logpriors,loglikelihoods,pSignal,Mtot,Mc,q,a1,a2,chiEffective,chiP,dist
    
    np.save('./corner_plot_data_ecc0.1', data)

    print data

if __name__=="__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-f',help="h5 file path")

    args = parser.parse_args()

    readSamples(args.f)
