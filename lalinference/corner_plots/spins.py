import numpy as np

def getMc(m1,m2):

    # Define symmetric mass ratio and total mass
    eta = (m1*m2)/np.power(m1+m2,2.)
    Mtot = m1+m2

    # Return chirp mass
    Mc = np.power(eta,3./5.)*Mtot
    return Mc

def getChiEffective(m1,m2,a1,a2,t1,t2):
    
    # Get spin components along orbital angular momentum
    a1z = a1*np.cos(t1)
    a2z = a2*np.cos(t2)
    
    # Chi effective
    chiEff = (m1*a1z+m2*a2z)/(m1+m2)
    return chiEff

def getChiP(m1,m2,a1,a2,t1,t2):

    # Helper functions
    A1 = 2.+3.*m2/(2.*m1)
    A2 = 2.+3.*m1/(2.*m2)
    S1p = m1**2.*a1*np.sin(t1)
    S2p = m2**2.*a2*np.sin(t2)

    #num = np.max([A1*S1p,A2*S2p])
    #if m1>m2:
    #    denom = A1*m1**2.
    #else:
    #    denom = A2*m2**2.

    # Spin numerator
    num = np.maximum(A1*S1p,A2*S2p)

    # Spin denominator
    denom = np.zeros(len(m1))
    m1_larger = np.where(m1>m2)
    m2_larger = np.where(m1<=m2)
    denom[m1_larger] = A1[m1_larger]*np.power(m1[m1_larger],2.)
    denom[m2_larger] = A2[m2_larger]*np.power(m2[m2_larger],2.)

    return num/denom
