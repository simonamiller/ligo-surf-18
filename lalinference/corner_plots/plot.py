import numpy as np 
import corner
import matplotlib as mpl
mpl.use('Agg')
from matplotlib import pyplot as plt


from matplotlib import rc
rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)
rc('xtick', labelsize=12) 
rc('ytick', labelsize=12)



# Uploading data
data = np.load('corner_plot_data_ecc0.1.npy')[()]


# ~~~ Ploting ~~~
to_plot = np.transpose([data['Mtot'], data['Mc'],data['chiEffective'], data['chiP']])

fig = corner.corner(to_plot, bins=30, labels=[r'$M_{tot}$',r'$M_{c}$',r'$\chi_{eff}$',r'$\chi_{P}$'])
axes = np.array(fig.axes).reshape((4, 4))

# 1d hist plot stuff
ax_mtot = axes[0,0]
ax_mc = axes[1,1]
ax_chieff = axes[2,2]
ax_chip = axes[3,3]

ax_mtot.set_xlim(25,55)
ax_chip.set_xlim(-0.2,1)

ax_mtot.axvline(30, color='b', linestyle='dashed', linewidth=1.0, label='injected value')
ax_mc.axvline(13.058, color='b', linestyle='dashed', linewidth=1.0)
ax_chieff.axvline(0, color='b', linestyle='dashed', linewidth=1.0)
ax_chip.axvline(0, color='b', linestyle='dashed', linewidth=1.0)

ax_mtot.legend(fontsize=12, bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

# mc and mtot
axes[1,0].set_xlim(25,55)
axes[1,0].axvline(30, color='b', linestyle='dashed', linewidth=1.0)
axes[1,0].axhline(13.058, color='b', linestyle='dashed', linewidth=1.0)

# chi_eff and mtot
axes[2,0].set_xlim(25,55)
axes[2,0].axvline(30, color='b', linestyle='dashed', linewidth=1.0)
axes[2,0].axhline(0, color='b', linestyle='dashed', linewidth=1.0)

# chi_p and mtot
axes[3,0].set_xlim(25,55)
axes[3,0].set_ylim(-0.2, 1.0)
axes[3,0].axvline(30, color='b', linestyle='dashed', linewidth=1.0)
axes[3,0].axhline(0, color='b', linestyle='dashed', linewidth=1.0)

# chi_eff and mc
axes[2,1].axvline(13.058, color='b', linestyle='dashed', linewidth=1.0)
axes[2,1].axhline(0, color='b', linestyle='dashed', linewidth=1.0)

# chi_p and mc
axes[3,1].set_ylim(-0.2, 1.0)
axes[3,1].axvline(13.058, color='b', linestyle='dashed', linewidth=1.0)
axes[3,1].axhline(0, color='b', linestyle='dashed', linewidth=1.0)

# chi_p and chi_eff
axes[3,2].set_ylim(-0.2, 1.0)
axes[3,2].axvline(0, color='b', linestyle='dashed', linewidth=1.0)
axes[3,2].axhline(0, color='b', linestyle='dashed', linewidth=1.0)


fig.suptitle(r'Injected $e_0$ = 0.1, Recovering with Spin Precession Waveform', fontsize=16)

plt.savefig('corner_plot_ecc0.1.png')


