import numpy as np

def getMc(m1,m2):

    # Define symmetric mass ratio and total mass
    eta = (m1*m2)/np.power(m1+m2,2.)
    Mtot = m1+m2

    # Return chirp mass
    Mc = np.power(eta,3./5.)*Mtot
    return Mc

def get_q(mc,mtot):

	eta = np.power(mc/mtot,5./3.)
	q = (1.-2.*eta-np.sqrt(1.-4.*eta))/(2.*eta)

	if np.any(q!=q):
		i = np.where(q!=q)
		print(len(mc),len(i[0]))
		#print (mc[i],mtot[i],np.power(mc[i]/mtot[i],5./3.))

	return q

def get_m1_m2_from_mc_q(mc,q):

	m1 = np.power(1.+q,1./5.)*mc/np.power(q,3./5.)
	m2 = q*m1
	return m1,m2

def get_mc_q_from_m1_m2(m1,m2):

	eta = (m1*m2)/np.power(m1+m2,2.)
	Mtot = m1+m2
	Mc = np.power(eta,3./5.)*Mtot

	if m1>m2:
		q = m2/m1
	else:
		q = m1/m2

	return Mc,q

