import argparse
import numpy as np
from decimal import * 

from pycbc.waveform import get_fd_waveform


def highFreqCutoff(mass1, mass2, eccentricity): 

    hp, hc = get_fd_waveform(approximant="EccentricFD",
                                mass1=mass1,
                                mass2=mass2,
                                delta_t=1.0/4096,
                                delta_f=0.001,
                                eccentricity=eccentricity,
                                f_lower=20)

    freq = hp.sample_frequencies[-1]

    print 'Cutoff frequency for m1={}, m2={}, e0={} is {} Hz'.format(mass1,mass2,eccentricity,freq)

    return freq



def cutoffPSD(oldfilepath, newfilepath, mass1, mass2, eccentricity): 

    psd = {}
    #cutoff_freq = 2200./float(mass) # mass to cutoff frequency conversion (maggiore eqn 4.40)
    cutoff_freq = highFreqCutoff(mass1,mass2,eccentricity)

    # Reading in old PSD or ASD: 

    freqs, psd_vals  = np.loadtxt(oldfilepath, dtype=np.float64, unpack=True)
    
    # key = frequency, value = psd or asd
    psd = {f:psd_vals[i] for i,f in enumerate(freqs)}    


    # Writing new PSD or ASD with high frequency cutoff:

    new = open(newfilepath, 'w') 
    
    for f in psd.keys(): 
        if f >= cutoff_freq: 
            psd[f] = 1 

    for f in sorted(psd.keys()): 
        line = str(f) + ' ' + str(psd[f]) + '\n'
        new.write(line)

    new.close() 



if __name__ == '__main__': 
    
    parser = argparse.ArgumentParser()
    parser.add_argument('-o',help='filepath for original PSD or ASD')
    parser.add_argument('-n',help='desired filepath for new PSD or ASD')
    parser.add_argument('-m1',help='mass1 (in solar mass units)')
    parser.add_argument('-m2',help='mass2 (in solar mass units)')
    parser.add_argument('-e0',help='initial eccentricity')
    args = parser.parse_args()

    cutoffPSD(args.o, args.n, float(args.m1), float(args.m2), float(args.e0))
   
    # Testing
    #highFreqCutoff(10, 10, 0.01)
    #highFreqCutoff(10, 10, 0.1)
    #highFreqCutoff(10, 10, 0.3)
    #highFreqCutoff(15, 15, 0.1)
    #highFreqCutoff(10, 15, 0.1)
    #highFreqCutoff(5, 5, 0.1)
    #highFreqCutoff(5, 10, 0.1)
