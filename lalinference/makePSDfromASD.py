import numpy as np
import argparse

def makePSD(asdFile,targetFile):

    freqs,asd = np.loadtxt(asdFile,unpack=True)
    psd = np.power(asd,2.)
    np.savetxt(targetFile,np.transpose([freqs,psd]))

if __name__=="__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-s',help='File containing asd')
    parser.add_argument('-t',help='Target output file in which to save psd')
    args = parser.parse_args()
    makePSD(args.s,args.t)
