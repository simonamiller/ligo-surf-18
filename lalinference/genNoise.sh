#!/bin/bash

pycbc_condition_strain \
    --fake-strain-from-file "LIGO-P1200087-v18-aLIGO_DESIGN.txt" \
    --fake-strain-seed 10 \
    --low-frequency-cutoff 10 \
    --sample-rate 4096 \
    --output-strain-file H-H1_GAUSSIAN-1173189118-1000.gwf \
    --channel-name H1:DCH-FAKE_STRAIN \
    --gps-start-time 1173189118 \
    --gps-end-time 1173190118

pycbc_condition_strain \
    --fake-strain-from-file "LIGO-P1200087-v18-aLIGO_DESIGN.txt" \
    --fake-strain-seed 12 \
    --low-frequency-cutoff 10 \
    --sample-rate 4096 \
    --output-strain-file L-L1_GAUSSIAN-1173189118-1000.gwf \
    --channel-name L1:DCH-FAKE_STRAIN \
    --gps-start-time 1173189118 \
    --gps-end-time 1173190118

