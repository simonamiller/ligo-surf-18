import numpy as np
from pycbc.psd.analytical import aLIGODesignSensitivityP1200087
from pycbc.noise import noise_from_psd
from pycbc.waveform import get_td_waveform
from pycbc.frame import write_frame
from pycbc.detector import Detector

from scipy.signal.windows import tukey

def Generate_ecc_gwf(filepath, channel, theta): # theta = eccentricity, mass, distance, f_lower, delta_f, delta_t, ra, deci, inclination, time, coa_phase, polarization

    eccentricity, mass, distance, f_lower, delta_f, delta_t, ra, dec, inclination, time, coa_phase, polarization = theta 

    start_time = 0.
    duration = 32.
    flen = int(2048 / delta_f) + 1
    tsamples = int(32 / delta_t)

    # Generate PSD: 
    psd = aLIGODesignSensitivityP1200087(flen, delta_f, f_lower)
    
    # Generate template:       
    hp, hc = get_td_waveform(approximant="EccentricTD",
                             mass1=mass,
                             mass2=mass,
                             eccentricity=eccentricity,
                             delta_t=delta_t,
                             delta_f=delta_f, 
                             f_lower=f_lower, 
                             distance=distance)

    # Projecting geocenter waveform onto detectors
    h = {}
    dets=['H1', 'L1']
    for det in dets: 
        ifo = Detector(det)
        fp, fc = ifo.antenna_pattern(ra, dec, polarization, time)
        ifo_h = fp*hp.copy() + fc*hc.copy()
        dt = ifo.time_delay_from_earth_center(ra, dec, time)
        #h[det] = ifo_h.cyclic_time_shift(dt)
        #h[det].start_time = start_time
        ifo_h._sample_times = ifo_h.sample_times + dt
        h[det] = ifo_h
    
    for i, det in enumerate(dets):

        strain = h[det]
     
        # Window for template:
        """
        window_template = tukey(len(strain), alpha=0.03)
        for i in range(len(strain)):
            strain[i] = strain[i]*window_template[i]
        """
    
        # Resize the signal buffer, and shift start time:
        """
        strain.resize(int(duration/delta_t))
        strain = strain.cyclic_time_shift(strain.start_time - duration/2.0)
        strain.start_time = start_time
        """

        # Generate noise:
        noise = noise_from_psd(tsamples, delta_t, psd, seed=np.random.randint(100))
        duration=noise.duration
        start_time=noise.start_time

        print(noise.sample_rate)
        print(strain.sample_rate)

        # Add template to noise = data: 
        data = strain + noise
        #data = noise.add_into(strain)
    
        # Window for data:
        window_noise = tukey(len(data), alpha=0.03)
        for i in range(len(data)):
            data[i] = data[i]*window_noise[i]
   
        # Writing gwf file:
        write_frame(filepath[i], channel[i], data)

if __name__=="__main__":

    # Default test values
    filepath=['./testFrame1.gwf', './testFrame2.gwf']
    channel=['H1:STRAIN', 'L1:STRAIN']
    ecc = 0.4
    mass = 10.
    dist = 50.
    f_lower = 20.
    delta_f = 0.1
    delta_t = 1./512.
    ra = 0 
    dec = 0 
    incl = 0 
    time = 5
    coa_phase = 0
    polarization = 0
    theta = np.array([ecc,mass,dist,f_lower,delta_f,delta_t,ra,dec,incl,time,coa_phase,polarization])

    Generate_ecc_gwf(filepath,channel,theta)
