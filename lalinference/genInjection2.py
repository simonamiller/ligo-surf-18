import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import lal
from pycbc.waveform import get_td_waveform
from pycbc.detector import Detector
from pycbc.types.timeseries import TimeSeries
#from gwpy.timeseries import TimeSeries
from pycbc.psd.analytical import aLIGODesignSensitivityP1200087
from pycbc.noise import noise_from_psd
from pycbc.frame import write_frame
from pycbc.frame.frame import read_frame
from pycbc.types import float32
from pycbc.filter.resample import resample_to_delta_t
from pycbc.filter.matchedfilter import matched_filter
from pycbc.noise import noise_from_psd

# https://pycbc.org/pycbc/latest/html/waveform.html

def SNRscaling(SNR, signal, f_lower):
    # signal projected at a distance of 1 mpc
    psd = aLIGODesignSensitivityP1200087(len(signal), signal.delta_f, f_lower)
    SNR0 = max(np.absolute(matched_filter(signal,signal,psd=psd,low_frequency_cutoff=f_lower)))
    scalefactor = float(SNR)/SNR0
    print 'Amplitude scaling factor for SNR of {} = {}'.format(SNR, scalefactor)
    return scalefactor

def genInjection(theta, noiseH1_filepath, noiseL1_filepath, filepath_H1, filepath_L1, channel_H1, channel_L1):

    # Import Noise .gwf file
    noise_H1 = read_frame(noiseH1_filepath, channel_H1)
    noise_L1 = read_frame(noiseL1_filepath, channel_L1)

    #delta_t = 1./2048
    #noise_H1 = resample_to_delta_t(noise_H1, delta_t)
    #noise_L1 = resample_to_delta_t(noise_L1, delta_t)

    """
    samplingRate = 2048
    duration = 100
    df = 1e-1
    """

    # Parameters
    fLow = 20
    mass1 = theta['mass1']
    mass2 = theta['mass2']
    eccentricity = theta['eccentricity']
    SNR = theta['SNR']
    declination = theta['declination']
    right_ascension = theta['right_ascension']
    polarization = theta['polarization']
    delta_t = noise_H1.delta_t
    
    # Generate waveform
    hp,hc = get_td_waveform(approximant="EccentricTD",\
                mass1=mass1,\
                mass2=mass2,\
                eccentricity=eccentricity,\
                delta_t = delta_t,\
                f_lower = fLow,\
                distance = 1.)

    # Create detector objects
    det_H1 = Detector('H1')
    det_L1 = Detector('L1')

    # Adjust time
    #end_time = noise_H1.end_time - 500 
    end_time = 1173189618
    hp.start_time += end_time
    hc.start_time += end_time

    # Project onto detectors
    signal_H1 = det_H1.project_wave(hp, hc,  right_ascension, declination, polarization)
    signal_L1 = det_L1.project_wave(hp, hc,  right_ascension, declination, polarization)

    # Read off start times and compute durations
    i_H1_dur = len(signal_H1)
    i_L1_dur = len(signal_L1)
    i_H1_start = int((float(signal_H1.start_time) - float(noise_H1.start_time))/noise_H1.delta_t)
    i_L1_start = int((float(signal_L1.start_time) - float(noise_L1.start_time))/noise_L1.delta_t)

    # Scaling for appropriate SNR
    scalefactor = SNRscaling(SNR, signal_H1, fLow)

    # Add signals into noise
    noise_H1[i_H1_start:i_H1_start+i_H1_dur] += scalefactor*np.array(signal_H1)
    noise_L1[i_L1_start:i_L1_start+i_L1_dur] += scalefactor*np.array(signal_L1)

    # Plotting
    fig,ax = plt.subplots(figsize=(8,3))
    ax.plot(noise_L1.sample_times,noise_L1,label='L1')
    ax.plot(noise_H1.sample_times,noise_H1,label='H1')
    xmin = float(end_time)-20.
    xmax = float(end_time)+10.
    ax.set_xlim(xmin,xmax)
    ax.legend()
    fig.tight_layout()
    fig.savefig('./injection.pdf',bbox_inches='tight')

    # Writing gwf files
    duration = len(noise_H1)*noise_H1.delta_t
    write_frame("{0}-{1}-{2}.gwf".format(filepath_H1,noise_H1.start_time,int(duration)), channel_H1, noise_H1)
    write_frame("{0}-{1}-{2}.gwf".format(filepath_L1,noise_L1.start_time,int(duration)), channel_L1, noise_L1)

if __name__=="__main__": 

    noiseH1_filepath = './H-H1_GAUSSIAN-1173189118-1000.gwf'
    noiseL1_filepath = './L-L1_GAUSSIAN-1173189118-1000.gwf'
    filepath_H1 = './lalinferencerun_ecc0.1/H-H1_TEST'
    filepath_L1 = './lalinferencerun_ecc0.1/L-L1_TEST'
    channel_H1 = 'H1:DCH-FAKE_STRAIN'
    channel_L1 = 'L1:DCH-FAKE_STRAIN'

    theta = {}
    theta['mass1'] = 15.
    theta['mass2'] = 15.
    theta['eccentricity'] = 0.1
    #theta['SNR'] = 10./np.sqrt(2)
    theta['SNR'] = 10
    theta['declination'] = 0
    theta['right_ascension'] = 0  
    theta['polarization'] = 0

    genInjection(theta, noiseH1_filepath, noiseL1_filepath, filepath_H1, filepath_L1, channel_H1, channel_L1)
