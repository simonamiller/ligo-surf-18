import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import lal
from pycbc.waveform import get_td_waveform
from pycbc.detector import Detector
from pycbc.types.timeseries import TimeSeries
from pycbc.psd.analytical import aLIGODesignSensitivityP1200087
from pycbc.noise import noise_from_psd
from pycbc.frame import write_frame
from pycbc.types import float32

# https://pycbc.org/pycbc/latest/html/waveform.html

def genInjection(theta, filepath_H1, filepath_L1, channel_H1, channel_L1):

    samplingRate = 1000
    duration = 100
    fLow = 20
    df = 1e-1
    delta_t = 1e-3

    # Parameters
    mass1 = theta['mass1']
    mass2 = theta['mass2']
    eccentricity = theta['eccentricity']
    distance = theta['distance']
    declination = theta['declination']
    right_ascension = theta['right_ascension']
    polarization = theta['polarization']

    # Generate waveform
    hp,hc = get_td_waveform(approximant="EccentricTD",\
                mass1=mass1,\
                mass2=mass2,\
                eccentricity=eccentricity,\
                delta_t = 1./samplingRate,\
                f_lower = fLow,\
                distance = distance)

    # Create detector objects
    det_H1 = Detector('H1')
    det_L1 = Detector('L1')

    # Choose time
    end_time = 5000000
    hp.start_time += end_time
    hc.start_time += end_time

    # Project onto detectors
    signal_H1 = det_H1.project_wave(hp, hc,  right_ascension, declination, polarization)
    signal_L1 = det_L1.project_wave(hp, hc,  right_ascension, declination, polarization)
    #signal_H1 = signal_H1.astype(float32)
    #signal_L1 = signal_L1.astype(float32)
    #signal_H1 = signal_H1.lal()
    #signal_L1 = signal_L1.lal()

    # Generate PSD
    fMax = samplingRate
    fLen = int((fMax-fLow)/df)
    psd = aLIGODesignSensitivityP1200087(fLen,df,fLow)

    # Simulate two noise instantiations
    tLen = int(duration*samplingRate)
    noise_H1 = noise_from_psd(tLen,1./samplingRate,psd)
    noise_L1 = noise_from_psd(tLen,1./samplingRate,psd)
    #noise_H1 = noise_H1.astype(float32)
    #noise_L1 = noise_L1.astype(float32)
    #noise_H1 = noise_H1.lal()
    #noise_L1 = noise_L1.lal()
    noise_H1.start_time += end_time-duration/2
    noise_L1.start_time += end_time-duration/2

    # Add signal into detector noise
    part_H1 = noise_H1.time_slice(signal_H1.start_time,signal_H1.end_time)
    part_L1 = noise_L1.time_slice(signal_L1.start_time,signal_L1.end_time)
    part_H1 += signal_H1
    part_L1 += signal_L1
    
    # Plotting
    fig,ax = plt.subplots(figsize=(8,3))
    ax.plot(noise_H1.sample_times,noise_H1,label='H1')
    ax.plot(noise_L1.sample_times,noise_L1,label='L1')
    ax.set_xlim(end_time-4.,end_time)
    ax.legend()
    fig.tight_layout()
    fig.savefig('./test.pdf',bbox_inches='tight')
<<<<<<< HEAD

    # Writing gwf files
    write_frame("{0}-{1}-{2}.gwf".format(filepath_H1,noise_H1.start_time,int(duration)), channel_H1, noise_H1)
    write_frame("{0}-{1}-{2}.gwf".format(filepath_L1,noise_L1.start_time,int(duration)), channel_L1, noise_L1)
=======
    
    # Writing gwf files
    write_frame(filepath_H1, channel_H1, noise_H1)
    write_frame(filepath_L1, channel_L1, noise_L1)
>>>>>>> 0b4e77ce3ed3b3b2c475199e1c4ba36b10dfaa68



if __name__=="__main__": 

    filepath_H1 = './H-H1_HOFT'
    filepath_L1 = './L-L1_HOFT'
    channel_H1 = 'H1:STRAIN'
    channel_L1 = 'L1:STRAIN'

    theta = {}
    theta['mass1'] = 10.
    theta['mass2'] = 10.
    theta['eccentricity'] = 0.1
    theta['distance'] = 30.
    theta['declination'] = 0
    theta['right_ascension'] = 0  
    theta['polarization'] = 0

    genInjection(theta, filepath_H1, filepath_L1, channel_H1, channel_L1)
