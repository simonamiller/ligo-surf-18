import numpy as np
import argparse
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

def plotPSD(txtfile,figname):

    # Load data 
    freqs, psd = np.loadtxt(txtfile, unpack=True)

    # Plot data
    plt.loglog(freqs, psd)
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('PSD')
    #plt.ylabel('ASD')
    #plt.xlim(0,300)
    plt.savefig(figname)

if __name__=="__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-s',help='Source frame file')
    parser.add_argument('-t',help='Target figure name')
    args = parser.parse_args()

    plotPSD(args.s,args.t)

 
