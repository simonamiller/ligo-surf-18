
# # Recovering eccentric 'signal' with eccentric waveform template - likelihood distribution over 2D parameter space of eccentricity and mass
# 
# Simona Miller, LIGO SURF 2018 (Last edited: August 8, 2018)


import numpy as np
import argparse
import matplotlib as mpl
mpl.use('Agg')
from matplotlib import pyplot as plt

from pycbc import psd as pypsd
from pycbc.waveform.generator import FDomainDetFrameGenerator, FDomainCBCGenerator
from pycbc import inference

from matplotlib import rc
rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)
rc('xtick', labelsize=14) 
rc('ytick', labelsize=14)


# Global variables
seglen = 10
sample_rate = 2048
N = seglen*sample_rate/2+1
fmin = 20.

tsig, ra, dec, pol = 3.1, 1.37, -1.26, 2.76


# Waveform generator
generator = FDomainDetFrameGenerator(FDomainCBCGenerator, 
                                     0., 
                                     variable_args=['eccentricity', 'mass1', 'mass2', 'distance'], 
                                     detectors=['H1', 'L1'], 
                                     delta_f=1./seglen, 
                                     f_lower=fmin, 
                                     approximant='EccentricFD',  
                                     tc=tsig, 
                                     ra=ra, 
                                     dec=dec, 
                                     polarization=pol)

# PSD
psd = pypsd.analytical.aLIGODesignSensitivityP1200087(N, 1./seglen, 20.)
psds = {'H1': psd, 'L1': psd}


# ## Necessary Helper Functions

def SNRtoDistance(SNR, ecc, mass, psd=psd, f_lower=fmin):

    data0 = generator.generate(eccentricity=ecc, mass1=mass, mass2=mass, distance=1)
    
    likelihood_eval = inference.GaussianLikelihood(['eccentricity'],
                                                       generator, data0, fmin, psds=psds, return_meta=False)
    SNR0 = likelihood_eval.snr()
        
    distance = int(float(SNR0)/SNR)
    
    return distance


def Normalize(lnlikelihood, dx, dy): 
    
    print '-- Normalization Routine -- '
    print 'Calculating lnlik...'
    maxll = max(lnlikelihood)
    lnlik = [x-maxll+20 for x in lnlikelihood]
    
    print 'Summing...'
    norm_sum = np.sum(np.exp(lnlik[:]))
    
    print 'Calculating norm constant...'
    norm_const = 1.0/(norm_sum*dy*dx)
    
    print 'Normalizing...'
    norm = [norm_const*np.exp(x) for x in lnlik]
 
    print 'Data Normalized!'
    return norm 



# ## Generate Likelihood Distributions

def genLikelihoodDist(mass, e0, SNR, dm, de, dot):   

    # SNR to distance
    dist = SNRtoDistance(SNR, e0, mass)
    
    # Generate Signal
    signal = generator.generate(eccentricity=e0, mass1=mass, mass2=mass, distance=dist)

    # Likelihood evaluator
    likelihood_eval = inference.GaussianLikelihood(['eccentricity', 'mass1', 'mass2'],
                                               generator, signal, fmin, psds=psds, return_meta=False)

    # Range of eccentricity and mass to sample over
    ecc_range = np.arange(e0-0.015, e0+0.015, de)
    mass_range = np.arange(mass-0.25, mass+0.25, dm)
    
    # Calculate likelihood distibution
    lnlik = {}
    for e in ecc_range:
        for m in mass_range:
            lnlik[(e,m)] = likelihood_eval.loglr(eccentricity=e, mass1=m, mass2=m)
            print 'Evaled for e={} m={}'.format(e,m)

    # Normalization
    lnlikelihood_arr=[]
    for e in ecc_range: 
        for m in mass_range:
            lnlikelihood_arr.append(lnlik[(e,m)])
    normalized_arr = Normalize(lnlikelihood_arr, de, dm)
    normalized = {}
    i=0
    for e in ecc_range: 
        for m in mass_range:  
            normalized[(e,m)]=normalized_arr[i]
            i+=1
    
    print 'Generated distribution for m={}, SNR={}, e0={}'.format(mass,SNR,e0)


    # Plotting
    print 'Plotting...'

    ecc_plot = np.transpose(normalized.keys())[0]
    mass_plot = np.transpose(normalized.keys())[1]
    likelihood_plot = normalized.values()
    
    plt.figure(figsize=(10,7))
    plt.title(r'$e_0$ = {} , $m_1$ = $m_2$ = {} $M_\odot$, SNR = {}'.format(e0, mass, SNR), fontsize=18)
    plt.xlabel('eccentricity', fontsize=16)
    plt.ylabel('mass', fontsize=16)
    
    plt.scatter(ecc_plot, mass_plot, c=likelihood_plot, s=dot, linewidths=0)
    plt.axvline(x=e0, color='white', linewidth=1.0, linestyle='dashed')
    plt.axhline(y=mass, color='white', linewidth=1.0, linestyle='dashed')
    plt.xlim(e0-0.01,e0+0.01)
    plt.ylim(mass-0.2,mass+0.2)
    
    cb = plt.colorbar()
    cb.set_label(label=r'$\mathcal{L}$',fontsize=16)
    
    plt.savefig('likelihood_pycbc_2D_m{}_SNR{}_e{}.png'.format(mass,SNR,e0))




if __name__=='__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-m',help='mass (m1=m2=mass)')
    parser.add_argument('-s',help='SNR')
    parser.add_argument('-e',help='e0')
    parser.add_argument('-dm',help='differential mass step')
    parser.add_argument('-de',help='differential eccentricity step')
    parser.add_argument('-dot',help='dot size on scatterplot')
    args = parser.parse_args()

    mass = int(args.m)
    e0 = float(args.e)
    SNR = int(args.s)
    dm = float(args.dm)
    de = float(args.de)
    dot = float(args.dot)

    print 'm1=m2={}, e0={}, SNR={}, dm={}, de={}, dotsize={}'.format(mass,e0,SNR,dm,de,dot)

    genLikelihoodDist(mass, e0, SNR, dm, de, dot)   
