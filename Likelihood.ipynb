{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Calculating likelihood between two waveforms \n",
    "\n",
    "Simona Miller, LIGO SURF Summer 2018\n",
    "(Last edited: July 10, 2018)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "%%capture output\n",
    "%run Overlap.ipynb"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Posterior density function:** \n",
    "\n",
    "$$\n",
    "p(\\vec{\\theta}\\,|\\,d ) = \\frac{p(\\vec{\\theta})\\,p(d\\,|\\,\\vec{\\theta})}{p(d)},\n",
    "$$\n",
    "\n",
    "where $\\vec{\\theta}$ is a vector containing unknown parameters and $ d = \\{d_1, d_2, \\ldots, d_{\\mathrm{N}_f}\\} $ is the data, aka strain measurements in $\\mathrm{N}_f$ discrete frequency bins.\n",
    "\n",
    "**Likelihood function:**\n",
    "\n",
    "$$\n",
    "p(d\\,|\\,\\vec{\\theta}) = \\exp \\left(-\\frac{1}{2}\\langle h(\\vec{\\theta})-d\\,|\\,h(\\vec{\\theta})-d\\rangle\\right),\n",
    "$$\n",
    "\n",
    "where the noise weighted inner product $\\langle \\,a\\,|\\,b\\,\\rangle$ is equal to: \n",
    "\n",
    "$$\n",
    "\\langle \\,a\\,|\\,b\\,\\rangle = 4 \\Re \\int_{0}^{\\infty} \\frac{\\tilde{a}(f)\\,\\tilde{b^*}(f)}{S_n(f)} df\n",
    "$$\n",
    "\n",
    "**Likelihood ratio:**\n",
    "\n",
    "$$\n",
    "Likelihood\\,\\,ratio = \\frac{p(d\\,|\\,\\vec{\\theta})}{p(d\\,|\\,n)}\n",
    "$$\n",
    "\n",
    "where $p(d\\,|\\,n)$ is the likelihood of the data being noise only, which reduces to:\n",
    "\n",
    "$$\n",
    "p(d\\,|\\,n) = \\exp \\left( -\\frac{1}{2}\\sum_i^{\\mathrm{N}_f}\\langle d_i\\,|\\,d_i\\rangle\\right),\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Function to calculate the noise weighted inner product of two vectors\n",
    "# a and b (FFTs) with noise vector Sn (PSD); df = freq. bin size\n",
    "# Note: a, b, and Sn must be same length\n",
    "\n",
    "def InnerProduct(a, b, Sn, df):\n",
    "    \n",
    "    b_conj = np.conj(b)\n",
    "    \n",
    "    integrand = []\n",
    "    \n",
    "    for i in range(0,len(a)):\n",
    "        intgrnd = a[i]*b_conj[i]/Sn[i]\n",
    "        integrand.append(intgrnd)\n",
    "            \n",
    "    result_complex = 4*np.sum(integrand)*df\n",
    "    \n",
    "    result = np.real(result_complex)\n",
    "    \n",
    "    return result"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Function to calculate the likelihood of data given parameters theta that\n",
    "# generate strain h_theta (frequency series) with noise Sn (PSD); df = freq. bin size\n",
    "# Note: h_theta, data, and Sn must all be same length\n",
    "\n",
    "def LogLikelihood(data, h_theta, Sn, df):\n",
    "    \n",
    "    h_minus_d = h_theta - data\n",
    "    \n",
    "    result = (-1.0/2)*InnerProduct(h_minus_d, h_minus_d, Sn, df) \n",
    "    \n",
    "    return result"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Function to calculate likelihood that data is just noise (Sn); df = freq. bin size\n",
    "# Note: data and Sn must be same length\n",
    "\n",
    "def LogLikelihoodNoise(data, Sn, df):\n",
    "    \n",
    "    result = (-1.0/2)*InnerProduct(data, data, Sn, df)\n",
    "        \n",
    "    return result"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Function to calculate the likelihood ratio: likelihood of data (data) given parameters theta \n",
    "# (generating strain h_theta) over likelihood that data is just noise (Sn); df = freq. bin size\n",
    "# Note: h_theta, data, and Sn must all be same length\n",
    "\n",
    "def LogLikelihoodRatio(data, h_theta, Sn, df):\n",
    "    \n",
    "    loglikelihood = LogLikelihood(data, h_theta, Sn, df)\n",
    "    \n",
    "    loglikelihood_noise = LogLikelihoodNoise(data, Sn, df)\n",
    "    \n",
    "    loglikelihood_ratio = loglikelihood - loglikelihood_noise\n",
    "    \n",
    "    return loglikelihood_ratio"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## LOG Likelihood for waveforms with different eccentricities"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "ecc = [0.000001, 0.00001, 0.0001, 0.001, 0.01, 0.1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "waveforms_hp = {} # dict for h+ waveforms\n",
    "waveforms_hc = {} # dict for hx waveforms\n",
    "\n",
    "for e in ecc: \n",
    "    \n",
    "    key = e\n",
    "    \n",
    "    hp, hc = get_fd_waveform(approximant=\"EccentricFD\",\n",
    "                         mass1=10,\n",
    "                         mass2=10,\n",
    "                         delta_t=1.0/8192,\n",
    "                         eccentricity=e,\n",
    "                         delta_f=0.001,\n",
    "                         f_lower=f_lower, \n",
    "                         f_upper=f_upper)\n",
    "    \n",
    "    waveforms_hp[key] = hp\n",
    "    waveforms_hc[key] = hc"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Log Likelihood of waveforms with e=0.000001 and e=1e-06:\n",
      " h+:-0.0\t hx:-0.0\n",
      "\n",
      "\n",
      "Log Likelihood of waveforms with e=0.000001 and e=1e-05:\n",
      " h+:-0.00137058446396\t hx:-0.00137058446395\n",
      "\n",
      "\n",
      "Log Likelihood of waveforms with e=0.000001 and e=0.0001:\n",
      " h+:-0.165835710977\t hx:-0.165835710973\n",
      "\n",
      "\n",
      "Log Likelihood of waveforms with e=0.000001 and e=0.001:\n",
      " h+:-16.933612951\t hx:-16.9336129131\n",
      "\n",
      "\n",
      "Log Likelihood of waveforms with e=0.000001 and e=0.01:\n",
      " h+:-2267.16249703\t hx:-2267.16213849\n",
      "\n",
      "\n",
      "Log Likelihood of waveforms with e=0.000001 and e=0.1:\n",
      " h+:-2807725.94185\t hx:-2807729.54106\n",
      "\n",
      "\n"
     ]
    }
   ],
   "source": [
    "for e in ecc: \n",
    "    \n",
    "    no_ecc_plus = waveforms_hp[ecc[0]]\n",
    "    no_ecc_cross = waveforms_hc[ecc[0]]\n",
    "    \n",
    "    ecc_plus = waveforms_hp[e]\n",
    "    ecc_cross = waveforms_hc[e]\n",
    "    \n",
    "    # h+ likelihood with e=0.000001 and e=e\n",
    "    # data ~ eccentric waveform; h_theta (template) ~ non-eccentric waveform\n",
    "    l_plus = LogLikelihood(ecc_plus, no_ecc_plus, noise_interp_trimmed, df)\n",
    "    \n",
    "    # hx likelihood with e=0.000001 and e=e\n",
    "    # data ~ eccentric waveform; h_theta (template) ~ non-eccentric waveform\n",
    "    l_cross = LogLikelihood(ecc_cross, no_ecc_cross, noise_interp_trimmed, df)\n",
    "    \n",
    "    print \"Log Likelihood of waveforms with e=0.000001 and e={}:\\n h+:{}\\t hx:{}\\n\\n\".format(e,l_plus,l_cross)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Likelihood for waveforms with different eccentricities"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Likelihood of waveforms with e=0.000001 and e=1e-06:\n",
      " h+:1.0\t hx:1.0\n",
      "\n",
      "\n",
      "Likelihood of waveforms with e=0.000001 and e=1e-05:\n",
      " h+:0.998630354358\t hx:0.998630354358\n",
      "\n",
      "\n",
      "Likelihood of waveforms with e=0.000001 and e=0.0001:\n",
      " h+:0.84718540602\t hx:0.847185406023\n",
      "\n",
      "\n",
      "Likelihood of waveforms with e=0.000001 and e=0.001:\n",
      " h+:4.42410409219e-08\t hx:4.42410425996e-08\n",
      "\n",
      "\n",
      "Likelihood of waveforms with e=0.000001 and e=0.01:\n",
      " h+:0.0\t hx:0.0\n",
      "\n",
      "\n",
      "Likelihood of waveforms with e=0.000001 and e=0.1:\n",
      " h+:0.0\t hx:0.0\n",
      "\n",
      "\n"
     ]
    }
   ],
   "source": [
    "for e in ecc: \n",
    "    \n",
    "    no_ecc_plus = waveforms_hp[ecc[0]]\n",
    "    no_ecc_cross = waveforms_hc[ecc[0]]\n",
    "    \n",
    "    ecc_plus = waveforms_hp[e]\n",
    "    ecc_cross = waveforms_hc[e]\n",
    "    \n",
    "    # h+ likelihood with e=0.000001 and e=e\n",
    "    # data ~ eccentric waveform; h_theta (template) ~ non-eccentric waveform\n",
    "    l_plus = np.exp(LogLikelihood(ecc_plus, no_ecc_plus, noise_interp_trimmed, df))\n",
    "    \n",
    "    # hx likelihood with e=0.000001 and e=e\n",
    "    # data ~ eccentric waveform; h_theta (template) ~ non-eccentric waveform\n",
    "    l_cross = np.exp(LogLikelihood(ecc_cross, no_ecc_cross, noise_interp_trimmed, df))\n",
    "    \n",
    "    print \"Likelihood of waveforms with e=0.000001 and e={}:\\n h+:{}\\t hx:{}\\n\\n\".format(e,l_plus,l_cross)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## LOG Likelihood that waveforms with different eccentricities are just noise"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Log Likelihood of that waveform is noise for e=1e-06:\n",
      " h+:-20597817.9691\t hx:-20597817.9691\n",
      "\n",
      "\n",
      "Log Likelihood of that waveform is noise for e=1e-05:\n",
      " h+:-20597822.0617\t hx:-20597822.0617\n",
      "\n",
      "\n",
      "Log Likelihood of that waveform is noise for e=0.0001:\n",
      " h+:-20597862.944\t hx:-20597862.944\n",
      "\n",
      "\n",
      "Log Likelihood of that waveform is noise for e=0.001:\n",
      " h+:-20598267.4338\t hx:-20598267.4338\n",
      "\n",
      "\n",
      "Log Likelihood of that waveform is noise for e=0.01:\n",
      " h+:-20601891.2955\t hx:-20601891.2984\n",
      "\n",
      "\n",
      "Log Likelihood of that waveform is noise for e=0.1:\n",
      " h+:-20599791.5541\t hx:-20599801.9487\n",
      "\n",
      "\n"
     ]
    }
   ],
   "source": [
    "for e in ecc: \n",
    "    \n",
    "    plus = waveforms_hp[e]\n",
    "    cross = waveforms_hc[e]\n",
    "    \n",
    "    # h+ likelihood ratio of just noise with e=e\n",
    "    ln_plus = LogLikelihoodNoise(plus, noise_interp_trimmed, df)\n",
    "    \n",
    "    # hx likelihood ratio of just noise with e=e\n",
    "    ln_cross = LogLikelihoodNoise(cross, noise_interp_trimmed, df)\n",
    "    \n",
    "    print \"Log Likelihood of that waveform is noise for e={}:\\n h+:{}\\t hx:{}\\n\\n\".format(e,ln_plus,ln_cross)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## LOG Likelihood ratio for waveforms with different eccentricities"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Log Likelihood of waveforms with e=0.000001 and e=1e-06:\n",
      " h+:20597817.9691\t hx:20597817.9691\n",
      "\n",
      "\n",
      "Log Likelihood of waveforms with e=0.000001 and e=1e-05:\n",
      " h+:20597822.0603\t hx:20597822.0603\n",
      "\n",
      "\n",
      "Log Likelihood of waveforms with e=0.000001 and e=0.0001:\n",
      " h+:20597862.7782\t hx:20597862.7782\n",
      "\n",
      "\n",
      "Log Likelihood of waveforms with e=0.000001 and e=0.001:\n",
      " h+:20598250.5001\t hx:20598250.5001\n",
      "\n",
      "\n",
      "Log Likelihood of waveforms with e=0.000001 and e=0.01:\n",
      " h+:20599624.133\t hx:20599624.1363\n",
      "\n",
      "\n",
      "Log Likelihood of waveforms with e=0.000001 and e=0.1:\n",
      " h+:17792065.6123\t hx:17792072.4076\n",
      "\n",
      "\n"
     ]
    }
   ],
   "source": [
    "for e in ecc: \n",
    "    \n",
    "    no_ecc_plus = waveforms_hp[ecc[0]]\n",
    "    no_ecc_cross = waveforms_hc[ecc[0]]\n",
    "    \n",
    "    ecc_plus = waveforms_hp[e]\n",
    "    ecc_cross = waveforms_hc[e]\n",
    "    \n",
    "    # h+ likelihood ratio with e=0.000001 and e=e\n",
    "    # data ~ eccentric waveform; h_theta (template) ~ non-eccentric waveform\n",
    "    lr_plus = LogLikelihoodRatio(ecc_plus, no_ecc_plus, noise_interp_trimmed, df)\n",
    "    \n",
    "    # hx likelihood ratio with e=0.000001 and e=e\n",
    "    # data ~ eccentric waveform; h_theta (template) ~ non-eccentric waveform\n",
    "    lr_cross = LogLikelihoodRatio(ecc_cross, no_ecc_cross, noise_interp_trimmed, df)\n",
    "    \n",
    "    print \"Log Likelihood of waveforms with e=0.000001 and e={}:\\n h+:{}\\t hx:{}\\n\\n\".format(e,lr_plus,lr_cross)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
